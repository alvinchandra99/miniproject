import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <div id="footer">
        <div class="background">
          <div class="body">
            <div class="subscribe">
              <h3>Get Weekly Newsletter</h3>
              <form action="" method="post">
                <input type="text" value="" class="txtfield" />
                <input type="submit" value="" class="button" />
              </form>
            </div>
            <div class="posts">
              <h3>Latest Post</h3>
              <p>
                This is just a place holder, so you can see what the site would
                look like.
                <a href="">...</a>
                <span>12/05/2011</span>
              </p>
            </div>
            <div class="connect">
              <h3>Follow Us:</h3>
              <a href="" target="_blank" class="facebook"></a>
              <a href="" target="_blank" class="twitter"></a>
              <a href="" target="_blank" class="googleplus"></a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
