import React, { Component } from "react";
import Articles from "../components/Articles";

export default class Content extends Component {
  render() {
    return (
      <div id="body">
        <div id="contents">
          <ul id="articles">
            <Articles
              title="Why Us?"
              content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse et nunc non tortor venenatis faucibus. Nullam varius a augue fermentum."
              button="Read More"
            />
            <Articles
              title="On Sale"
              content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse et nunc non tortor venenatis faucibus. Nullam varius a augue fermentum."
              button="View All"
            />
            <Articles
              title="Get A Quote"
              content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse et nunc non tortor venenatis faucibus. Nullam varius a augue fermentum."
              button="Request"
            />
          </ul>
        </div>
      </div>
    );
  }
}
