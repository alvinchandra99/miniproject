import React from "react";
import AdboxTitle from "../components/AdboxTitle";
import AdboxItem from "../components/AdboxItem";
import MenShirt1 from "../assets/images/men-shirt1.jpg";
import WomanShirt1 from "../assets/images/woman-shirt1.jpg";
import MenShirt2 from "../assets/images/men-shirt2.jpg";

export default function Adbox() {
  return (
    <div id="adbox">
      <AdboxTitle title="Hot Shirts for this Month" />
      <ul>
        <AdboxItem imgUrl={MenShirt1} />
        <AdboxItem imgUrl={WomanShirt1} />
        <AdboxItem imgUrl={MenShirt2} />
      </ul>
    </div>
  );
}
