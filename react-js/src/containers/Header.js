import React, { Component } from "react";
import Logo from "../components/Logo";
import Navbar from "../components/Navbar";

export default class Header extends Component {
  render() {
    return (
      <div id="header">
        <div>
          <Navbar />
          <Logo />
        </div>
      </div>
    );
  }
}
