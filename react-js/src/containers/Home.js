import React, { Component } from "react";
import Header from "./Header";
import "../assets/css/style.css";
import Adbox from "./Adbox";
import Content from "./Content";
import Footer from "./Footer";

export default class Home extends Component {
  render() {
    return (
      <>
        <Header />
        <Adbox />
        <Content />
        <Footer />
      </>
    );
  }
}
