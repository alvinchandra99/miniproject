import React from "react";
import NavButton from "./NavButton";
import NavItem from "./NavItem";

export default function Navbar() {
  return (
    <div>
      <div id="navigation">
        <div class="infos">
          <NavButton button1="Cart" button2="0 items" />
        </div>
        <div>
          <NavButton button1="Login" button2="Register" />
        </div>
        <ul id="primary">
          <NavItem class="selected" menu="Home" />
          <NavItem menu="About" />
          <NavItem menu="Men" />
        </ul>
        <ul id="secondary">
          <NavItem menu="Women" />
          <NavItem menu="Blog" />
          <NavItem menu="Contact" />
        </ul>
      </div>
    </div>
  );
}
