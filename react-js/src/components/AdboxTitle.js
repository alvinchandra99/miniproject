import React from "react";

export default function AdboxTitle(props) {
  return <h1>{props.title}</h1>;
}
