import React from "react";

export default function Articles(props) {
  return (
    <li>
      <h1>{props.title}</h1>
      <p>{props.content}</p>
      <a href="" class="more">
        {props.button}
      </a>
    </li>
  );
}
