import React from "react";

export default function NavItem(props) {
  return (
    <li class={props.class}>
      <a href="">
        <span>{props.menu}</span>
      </a>
    </li>
  );
}
