import React from "react";
import MenShirt1 from "../assets/images/men-shirt1.jpg";

export default function AdboxItem(props) {
  return (
    <>
      <li>
        <a href="">
          <img src={props.imgUrl} alt="Img" />
        </a>
      </li>
      <li></li>
    </>
  );
}
