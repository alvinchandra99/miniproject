import React from "react";
import LogoImg from "./../assets/images/logo.png";

export default function Logo() {
  return (
    <a href="" id="logo">
      <img src={LogoImg} alt="LOGO" />
    </a>
  );
}
